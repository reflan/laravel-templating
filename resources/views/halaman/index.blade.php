@extends('layout.master')

@section('judul')
	Media Online
@endsection

@section('content')
	<h3>Sosial Media Developer</h3>
	<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
	<strong>Benefit Join di Media Online</strong>
	<ul>
		<li>Mendapatkan motivasi dari sesama para Developer</li>
		<li>Sharing knowlenge</li>
		<li>Dibuat oleh calon web developer terbaik</li>
	</ul>

	<strong>Cara Bergabung ke Media Online</strong>
	<ol>
		<li>Mengunjungi Website ini</li>
		<li>mendaftarkan di <a href="/register">Form Sign Up</a></li>
		<li>Selesai</li>
	</ol>
@endsection