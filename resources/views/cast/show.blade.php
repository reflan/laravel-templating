@extends('layout.master')

@section('judul')
	Detail Cast
@endsection

@section('content')
    <div class="form-group row">
        <label class="col-sm-2 col-form-label" >Nama</label>
        <div class="col-sm-10" style="margin-top:10px;">
        {{$cast->nama}}
        </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Umur</label>
      <div class="col-sm-10" style="margin-top:10px;">
      {{$cast->umur}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Bio</label>
      <div class="col-sm-10" style="margin-top:10px;">
      {{$cast->bio}}
      </div>
    </div>
    <a href="/cast" class="btn btn-primary">Kembali</a>
@endsection