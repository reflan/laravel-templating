<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/hello-laravel', function () {
//     echo "Ini adalah halaman baru <br>";
//     return 'hello laravel';
// });

Route::get('/', 'IndexController@Home');
Route::get('/register', 'AuthController@Register');
Route::post('/welcome', 'AuthController@Kirim');

Route::get('/data-tables', 'IndexController@Datatable');

Route::group(['middleware' => ['auth']], function () {
	// CRUD Cast
	// Create
	Route::get('/cast/create', 'CastController@create');
	Route::post('/cast', 'CastController@store');// --> route untuk proses insert ke db

	// Read
	Route::get('/cast', 'CastController@index'); //route tampil data
	Route::get('/cast/{cast_id}', 'CastController@show'); //route tampil detail

	// Update
	Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //route tampil edit
	Route::put('/cast/{cast_id}/', 'CastController@update'); //route untuk proses update

	// delete
	Route::delete('/cast/{cast_id}/', 'CastController@destroy'); //route untuk proses delete


	// update profile
	Route::Resource('profil','ProfileController')->only([
		'index','update'
	]);

});


// Route::Resource('cast','CastController');
Auth::routes();
